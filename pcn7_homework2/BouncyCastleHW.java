/*
 *	Homework 2 --> Bouncy Castle AES, Blowfish, RSA Comparison Tests
 *	Created by Philip Ni
 *	CS1653 -- William Garrison
 *	University of Pittsburgh
 *
 *
 *	Run with the following:
 *		javac -cp bcprov-jdk15on-151.jar:. BouncyCastle.java
 *		java -cp bcprov-jdk15on-151.jar:. BouncyCastle [args:crypto method]
 *
 *		-->[args:crypto method] (i.e. arguments) can be 'AES', 'BLOWFISH', 'RSA', or 'EXTRACREDIT'
 *		-->arguments are not case-sensitive
 *		-->run with no arguments to get 'help' screen
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.util.Random;
import java.util.Arrays;
import java.util.ArrayList;

import java.security.Security;
import java.security.Signature;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

@SuppressWarnings({"unchecked", "deprecation"})
public class BouncyCastleHW{
	//Used for testing
	private static final byte[] DEFAULT_IV = new byte[] {(byte)0x89, (byte)0x23, (byte)0x01, (byte)0x46, (byte)0x84, (byte)0x93, (byte)0x16, (byte)0x54, (byte)0x08, (byte)0x30, (byte)0x31, (byte)0x22, (byte)0x71, (byte)0xa5, (byte)0x1e, (byte)0x14};
	
	public static void main(String[] args){
		if(args.length == 1 && args != null && args[0] != null){
			//Cryto method is passed in as arguments
			String crypto_method = args[0];
			
			try{
				//Make sure the Bouncy Castle JRE is present and functioning properly
				Security.addProvider(new BouncyCastleProvider());
				System.out.println("Provider: " + Security.getProvider("BC"));
				
				//Ask for user input for plain text to encrypt and then decrypt
				System.out.println("Using: " + crypto_method.toUpperCase());
				BufferedReader inreader = new BufferedReader(new InputStreamReader(System.in));
				
				if(crypto_method.toUpperCase().equals("AES")){				//AES
					//AES encrypt/decrypt
					System.out.println("Enter a plaintext string to encode below (RSA limited to 86 characters): ");
					String plaintext = inreader.readLine();
					System.out.println();
					byte[] input = plaintext.getBytes();
					byte[] ivBytes = new byte[16]; //iv byte array of 16 bytes
					SecureRandom secrand = new SecureRandom();
					secrand.nextBytes(ivBytes); //use SecureRandom to get iv
					KeyGenerator kg = KeyGenerator.getInstance("AES", "BC");
					kg.init(128);
					
					//encryption
					Key enckey = kg.generateKey();
					Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
					cipher.init(Cipher.ENCRYPT_MODE, enckey, new IvParameterSpec(ivBytes));
					byte[] ciphertext = new byte[cipher.getOutputSize(input.length)];
					int clen = cipher.update(input, 0, input.length, ciphertext, 0);
					clen += cipher.doFinal(ciphertext, clen);
					System.out.println("Encrypted Cipher Text:\n" + new String(ciphertext));
					System.out.println();
					
					//decryption
					Key deckey = new SecretKeySpec(enckey.getEncoded(), enckey.getAlgorithm());
					cipher.init(Cipher.DECRYPT_MODE, deckey, new IvParameterSpec(ivBytes));
					byte[] decryptedtext = new byte[cipher.getOutputSize(clen)];
					int ptlen = cipher.update(ciphertext, 0, clen, decryptedtext, 0);
					ptlen += cipher.doFinal(decryptedtext, ptlen);
					System.out.println("Decrypted Plain Text:\n" + new String(decryptedtext));
					System.out.println();
				}
				else if(crypto_method.toUpperCase().equals("BLOWFISH")){	//Blowfish
					//Blowfish encrypt/decrypt
					System.out.println("Enter a plaintext string to encode below (RSA limited to 86 characters): ");
					String plaintext = inreader.readLine();
					System.out.println();
					byte[] input = plaintext.getBytes();
					byte[] ivBytes = new byte[8]; //iv byte array of 16 bytes
					SecureRandom secrand = new SecureRandom();
					secrand.nextBytes(ivBytes); //use SecureRandom to get iv
					KeyGenerator kg = KeyGenerator.getInstance("Blowfish");
					kg.init(128);
					
					//encryption
					Key enckey = kg.generateKey();
					Cipher cipher = Cipher.getInstance("Blowfish/CBC/PKCS7Padding", "BC");
					cipher.init(Cipher.ENCRYPT_MODE, enckey, new IvParameterSpec(ivBytes));
					byte[] ciphertext = new byte[cipher.getOutputSize(input.length)];
					int clen = cipher.update(input, 0, input.length, ciphertext, 0);
					clen += cipher.doFinal(ciphertext, clen);
					System.out.println("Encrypted Cipher Text:\n" + new String(ciphertext));
					System.out.println();
					
					//decryption
					Key deckey = new SecretKeySpec(enckey.getEncoded(), enckey.getAlgorithm());
					cipher.init(Cipher.DECRYPT_MODE, deckey, new IvParameterSpec(ivBytes));
					byte[] decryptedtext = new byte[cipher.getOutputSize(clen)];
					int ptlen = cipher.update(ciphertext, 0, clen, decryptedtext, 0);
					ptlen += cipher.doFinal(decryptedtext, ptlen);
					System.out.println("Decrypted Plain Text:\n" + new String(decryptedtext));
					System.out.println();
				}
				else if(crypto_method.toUpperCase().equals("RSA")){			//RSA
					//RSA encrypt/decrypt
					System.out.println("Enter a plaintext string to encode below (RSA limited to 86 characters): ");
					String plaintext = inreader.readLine();
					System.out.println();
					byte[] input = plaintext.getBytes();
					SecureRandom secrand = new SecureRandom();
					KeyPairGenerator kg = KeyPairGenerator.getInstance("RSA", "BC");
					kg.initialize(1024, secrand);
					KeyPair pair = kg.generateKeyPair();
					
					//ecryption
					Cipher cipher = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");
					Signature rsa_sig = Signature.getInstance("SHA1withRSA", "BC");
					Key pub = pair.getPublic();
					Key priv = pair.getPrivate();
					rsa_sig.initSign(pair.getPrivate(), new SecureRandom());
					byte[] inbyte = Arrays.copyOf(input, input.length); //sign original input string
					rsa_sig.update(inbyte);
					byte[] signature = rsa_sig.sign();
					System.out.println("RSA Signature:\n" + new String(signature) + "\n");
					cipher.init(Cipher.ENCRYPT_MODE, pub, secrand);
					byte[] ciphertext = cipher.doFinal(input);
					System.out.println("Encrypted Cipher Text:\n" + new String(ciphertext));
					System.out.println();
					
					//decryption
					cipher.init(Cipher.DECRYPT_MODE, priv);
					byte[] decryptedtext = cipher.doFinal(ciphertext);
					rsa_sig.initVerify(pair.getPublic());
					rsa_sig.update(inbyte);
					System.out.println("Decrypted Plain Text:\n" + new String(decryptedtext) + "\n");
					System.out.println("Verify Signature: " + rsa_sig.verify(signature));
					System.out.println();
				}
				else if(crypto_method.toUpperCase().equals("EXTRACREDIT")){	//Extra Credit
					System.out.println("Not yet implemented");
					//Perform extra credit stuff and display results to the screen
					/*
					}
					
					//AES test
					aesTime = System.nanoTime();
					for(String s: strlist){
					}
					
					//Blowfish test
					blowfishTime = System.nanoTime();
					for(String s: strlist){
					}
					
					//RSA test
					for(String s: strlist){
					}
					
					//print results
				}
				else{
					System.out.println("Cannot recognize commandline arguments. Run again with correct arguments. ");
					arguments();
					System.exit(0);
				}*/
			}
			catch(UnsupportedEncodingException uee){
				System.out.println(uee);
				System.exit(-1);
			}
			catch(NoSuchProviderException nspe){
				System.out.println(nspe);
				System.exit(-1);
			}
			catch(IllegalBlockSizeException ibse){
				System.out.println(ibse);
				System.exit(-1);
			}
			catch(InvalidAlgorithmParameterException iape){
				System.out.println(iape);
				System.exit(-1);
			}
			catch(InvalidKeyException ike){
				System.out.println(ike);
				System.exit(-1);
			}
			catch(ArrayIndexOutOfBoundsException aioobe){
				System.out.println(aioobe);
				System.exit(-1);
			}
			catch(NoSuchPaddingException nspe){
				System.out.println(nspe);
				System.exit(-1);
			}
			catch(BadPaddingException bpe){
				System.out.println(bpe);
				System.exit(-1);
			}
			catch(Exception e){
				System.out.println("\nCaught Exception:\n" + e);
				System.exit(-1);
			}
		}
		else{
			//Cannot recognize the arguments
			arguments();
			System.exit(0);
		}
	}
	
	public static void arguments(){
		System.out.println("Check arguments:");
		System.out.println("\t\'AES\'\t\t\tRuns AES test");
		System.out.println("\t\'BLOWFISH\'\t\tRuns Blowfish test");
		System.out.println("\t\'RSA\'\t\t\tRuns RSA test");
		System.out.println("\t\'EXTRACREDIT\'\t\tRuns extra credit assignment and prints the results");
		System.out.println("**Running with no arguments or incorrect arguments prints this message.");
		System.out.println("**Arguments are not case-sensitive.");
	}
}